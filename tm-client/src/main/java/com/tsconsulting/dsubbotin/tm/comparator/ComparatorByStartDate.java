package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasStartDate;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    public static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    @Override
    public int compare(@NotNull final IHasStartDate o1, @NotNull final IHasStartDate o2) {
        if (o1.getStartDate() == null) return 1;
        if (o2.getStartDate() == null) return -1;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
