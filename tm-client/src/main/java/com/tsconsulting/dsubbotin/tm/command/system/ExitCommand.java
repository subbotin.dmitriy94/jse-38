package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ExitCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "exit";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Exit application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
