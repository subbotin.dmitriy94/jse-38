package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTask";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String userId;

    @NotNull
    private final Connection connection;

    public TaskRepositoryTest() throws AbstractException, SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        taskRepository = new TaskRepository(connection);
        userRepository = new UserRepository(connection);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        userRepository.add(user);
        task = new Task();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        projectRepository = new ProjectRepository(connection);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName("Project");
        connection.commit();
    }

    @Before
    public void initializeTest() throws AbstractException, SQLException {
        taskRepository.add(userId, task);
        projectRepository.add(userId, project);
        connection.commit();
    }

    @Test
    public void findTask() throws AbstractException, SQLException {
        checkTask(taskRepository.findByName(userId, taskName));
        checkTask(taskRepository.findById(taskId));
        checkTask(taskRepository.findById(userId, taskId));
        checkTask(taskRepository.findByIndex(0));
    }

    private void checkTask(@NotNull final Task foundTask) {
        Assert.assertEquals(task.getId(), foundTask.getId());
        Assert.assertEquals(task.getName(), foundTask.getName());
        Assert.assertEquals(task.getDescription(), foundTask.getDescription());
        Assert.assertEquals(task.getUserId(), foundTask.getUserId());
        Assert.assertEquals(task.getStartDate(), foundTask.getStartDate());
    }

    @Test
    public void removeByName() throws AbstractException, SQLException {
        Assert.assertNotNull(task);
        taskRepository.removeByName(userId, taskName);
        connection.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void updateById() throws AbstractException, SQLException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateById(userId, taskId, newTaskName, newTaskDescription);
        connection.commit();
        @NotNull final Task updatedTask = taskRepository.findById(userId, taskId);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void updateByIndex() throws AbstractException, SQLException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateByIndex(userId, 0, newTaskName, newTaskDescription);
        connection.commit();
        @NotNull final Task updatedTask = taskRepository.findById(userId, taskId);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void startById() throws AbstractException, SQLException {
        taskRepository.startById(userId, taskId);
        connection.commit();
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException, SQLException {
        taskRepository.startByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException, SQLException {
        taskRepository.startByName(userId, taskName);
        connection.commit();
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException, SQLException {
        taskRepository.finishById(userId, taskId);
        connection.commit();
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException, SQLException {
        taskRepository.finishByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException, SQLException {
        taskRepository.finishByName(userId, taskName);
        connection.commit();
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException, SQLException {
        taskRepository.updateStatusById(userId, taskId, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusById(userId, taskId, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusById(userId, taskId, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException, SQLException {
        taskRepository.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByIndex(userId, 0, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException, SQLException {
        taskRepository.updateStatusByName(userId, taskName, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByName(userId, taskName, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByName(userId, taskName, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProjectById() throws AbstractException, SQLException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        connection.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskById() throws AbstractException, SQLException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        connection.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskById(userId, taskId);
        connection.commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void findAllByProjectId() throws AbstractException, SQLException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        task.setProjectId(projectId);
        connection.commit();
        @NotNull final Task tmpTask = taskRepository.findAllByProjectId(userId, projectId).get(0);
        Assert.assertEquals(task.getId(), tmpTask.getId());
        Assert.assertEquals(task.getName(), tmpTask.getName());
        Assert.assertEquals(task.getDescription(), tmpTask.getDescription());
        Assert.assertEquals(task.getProjectId(), tmpTask.getProjectId());
    }

    @Test
    public void removeAllTaskByProjectId() throws AbstractException, SQLException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final Task task1 = new Task();
        task1.setName("Task1");
        task1.setUserId(userId);
        taskRepository.add(userId, task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        connection.commit();
        Assert.assertEquals(2, taskRepository.findAllByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        connection.commit();
    }

    @After
    public void finalizeTest() throws SQLException, AbstractException {
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        userRepository.removeById(userId);
        connection.commit();
        connection.close();
    }

}
