package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

@RequiredArgsConstructor
public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String url = propertyService.getJdbcUrl();
        @NotNull final Properties properties = new Properties();
        properties.setProperty("user", propertyService.getJdbcLogin());
        properties.setProperty("password", propertyService.getJdbcPassword());
        @NotNull final Connection connection = DriverManager.getConnection(url, properties);
        connection.setAutoCommit(false);
        return connection;
    }

}
