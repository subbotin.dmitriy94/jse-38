package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.repository.UserRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final String email
    ) throws AbstractException {
        if (isLogin(login)) throw new UserFoundException();
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        user.setEmail(email);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.add(user);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return user;
    }

    @Override
    public void addAll(@NotNull List<User> users) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            for (@NotNull final User user : users) userRepository.add(user);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }

    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.findByLogin(login);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String password
    ) throws AbstractException {
        checkId(id);
        checkPassword(password);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.setPassword(id, getPasswordHash(password));
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void setRole(
            @NotNull final String id,
            @NotNull final Role role
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.setRole(id, role);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.updateById(id, lastName, firstName, middleName, email);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public boolean isLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.isLogin(login);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.lockByLogin(login);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            userRepository.unlockByLogin(login);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    private void checkLogin(@NotNull final String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(@NotNull final String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

    @NotNull
    private String getPasswordHash(@NotNull String password) {
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(iteration, secret, password);
    }

}