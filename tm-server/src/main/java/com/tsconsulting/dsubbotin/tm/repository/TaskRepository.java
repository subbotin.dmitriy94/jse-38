package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(EnumerationUtil.parseStatus(row.getString("status")));
        task.setCreateDate(row.getTimestamp("create_date"));
        task.setStartDate(row.getTimestamp("start_date"));
        return task;
    }

    @Override
    protected @NotNull String getTableName() {
        return "tasks";
    }

    @Override
    public void add(
            @NotNull String userId,
            @NotNull Task task
    ) throws AbstractException, SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, status, create_date, start_date, user_id, project_id)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(task.getCreateDate().getTime()));
        @Nullable final Date startDate = task.getStartDate();
        statement.setTimestamp(6, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setString(7, task.getUserId());
        statement.setString(8, task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @NotNull
    public Task findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? AND name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new TaskNotFoundException();
        @NotNull final Task task = fetch(resultSet);
        statement.close();
        return task;
    }

    @Override
    public void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.execute();
        statement.close();
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final String taskId = task.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException, SQLException {
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final String taskId = task.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.execute();
        statement.close();
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException, SQLException {
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final String taskId = task.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.execute();
        statement.close();
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.execute();
        statement.close();
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final Task task = findByIndex(userId, index);
        @NotNull final String taskId = task.getId();
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.execute();
        statement.close();
    }

    @Override
    public void unbindTaskById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? AND project_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new TaskNotFoundException();
        return result;
    }

    @Override
    public void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ?" +
                " WHERE user_id = ? and project_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

}
