package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project) throws AbstractException, SQLException;

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void removeByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException, SQLException;

    void updateByIndex(
            @NotNull String userId,
            int index,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException, SQLException;

    void startById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void startByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

    void startByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void finishById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void finishByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

    void finishByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void updateStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    ) throws AbstractException, SQLException;

    void updateStatusByIndex(
            @NotNull String userId,
            int index,
            @NotNull Status status
    ) throws AbstractException, SQLException;

    void updateStatusByName(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    ) throws AbstractException, SQLException;

}
