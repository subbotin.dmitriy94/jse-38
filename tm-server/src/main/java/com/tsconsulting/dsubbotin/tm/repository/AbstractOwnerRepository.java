package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        try {
            findById(userId, id);
            return true;
        } catch (AbstractException | SQLException exception) {
            return false;
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void clear(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new EntityNotFoundException();
        return result;
    }

    @Override
    @NotNull
    public List<E> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<E> comparator
    ) throws AbstractException, SQLException {
        List<E> entities = findAll(userId);
        entities.sort(comparator);
        return entities;
    }

    @Override
    @NotNull
    public E findById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    @NotNull
    public E findByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException, SQLException {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? LIMIT 1 OFFSET ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new IndexIncorrectException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException, SQLException {
        @NotNull final E entity = findByIndex(userId, index);
        @NotNull final String id = entity.getId();
        removeById(userId, id);
    }

}