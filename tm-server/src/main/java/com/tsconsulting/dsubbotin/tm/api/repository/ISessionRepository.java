package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session> {

    void open(@NotNull final Session session) throws SQLException;

    boolean close(@NotNull final Session session) throws SQLException;

    boolean contains(@NotNull final String id) throws SQLException;

}
