package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(userId, project);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return project;
    }

    @Override
    public void addAll(@NotNull List<Project> projects) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            for (@NotNull final Project project : projects) projectRepository.add(project.getUserId(), project);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public Project findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            return projectRepository.findByName(userId, name);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (index < 0) throw new EmptyIndexException();
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.startById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.startByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.startByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.finishById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }

    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.finishByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.finishByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.updateStatusById(userId, id, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.updateStatusByIndex(userId, index, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.updateStatusByName(userId, name, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

}
