package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IOwnerService;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    public AbstractOwnerService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    protected abstract IOwnerRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            return ownerRepository.existById(userId, id);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void remove(
            @NotNull final String userId,
            @NotNull final E entity
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            ownerRepository.remove(userId, entity);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public List<E> findAll(
            @NotNull final String userId,
            @Nullable final String sort
    ) throws AbstractException {
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            try (@NotNull final Connection connection = connectionService.getConnection()) {
                @Nullable IOwnerRepository<E> ownerRepository = getRepository(connection);
                return ownerRepository.findAll(userId, sortType.getComparator());
            } catch (SQLException exception) {
                logService.error(exception);
                throw new DatabaseOperationException();
            }
        } catch (UnknownSortException e) {
            try (@NotNull final Connection connection = connectionService.getConnection()) {
                @Nullable IOwnerRepository<E> ownerRepository = getRepository(connection);
                return ownerRepository.findAll(userId);
            } catch (SQLException exception) {
                logService.error(exception);
                throw new DatabaseOperationException();
            }
        }
    }

    @Override
    @NotNull
    public List<E> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<E> comparator
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            return ownerRepository.findAll(userId, comparator);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public E findById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            return ownerRepository.findById(userId, id);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public E findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            return ownerRepository.findByIndex(userId, index);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            ownerRepository.removeById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
            ownerRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

}
