package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task) throws AbstractException, SQLException;

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void removeByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException, SQLException;

    void updateByIndex(
            @NotNull String userId,
            int index,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException, SQLException;

    void startById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void startByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

    void startByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void finishById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    void finishByIndex(@NotNull String userId, int index) throws AbstractException, SQLException;

    void finishByName(@NotNull String userId, @NotNull String name) throws AbstractException, SQLException;

    void updateStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    ) throws AbstractException, SQLException;

    void updateStatusByIndex(
            @NotNull String userId,
            int index,
            @NotNull Status status
    ) throws AbstractException, SQLException;

    void updateStatusByName(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    ) throws AbstractException, SQLException;

    void bindTaskToProjectById(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) throws AbstractException, SQLException;

    void unbindTaskById(@NotNull String userId, @NotNull String id) throws AbstractException, SQLException;

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String id) throws SQLException, AbstractException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String id) throws SQLException;

}
