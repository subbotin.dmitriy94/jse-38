package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract E fetch(@NotNull final ResultSet row) throws AbstractException, SQLException;

    @NotNull
    protected abstract String getTableName();

    @Override
    public void remove(@NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + ";";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Override
    @NotNull
    public List<E> findAll() throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + ";";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<E> result = new ArrayList<>();
            while (resultSet.next()) result.add(fetch(resultSet));
            return result;
        }
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) throws AbstractException, SQLException {
        @NotNull final List<E> entitiesList = findAll();
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @Override
    @NotNull
    public E findById(@NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    @NotNull
    public E findByIndex(final int index) throws AbstractException, SQLException {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " LIMIT 1 OFFSET ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new IndexIncorrectException();
        @NotNull final E entity = fetch(resultSet);
        statement.close();
        return entity;
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException, SQLException {
        @NotNull final E entity = findByIndex(index);
        @NotNull final String id = entity.getId();
        removeById(id);
    }

}